// main36.cc is a part of the PYTHIA event generator.
// Copyright (C) 2021 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; DIS;

// Basic setup for Deeply Inelastic Scattering at HERA.

#include "Pythia8/Pythia.h"
#include <fstream>
#include <numeric> 

using namespace Pythia8;

int main() {

  // Generator. Shorthand for event.
  Pythia pythia;
  Event& event = pythia.event;
  Info info;
  Rndm rndm;


  // Pointer for parton distribution functions.
  PDFPtr PDF = make_shared<LHAGrid1>
              ( 2212, "13", "../share/Pythia8/pdfdata/", &info );

  // Beam energies, minimal Q2, number of events to generate.
  double eProton   = 920.;
  double eElectron = 27.5;
  double Q2min     = 1.;
  int    nEvent    = 10000;

  // Set up incoming beams, for frame with unequal beam energies.
  pythia.readString("Beams:frameType = 2");
  // BeamA = proton. If you swap these, remember to swap event indices below as well. 
  pythia.readString("Beams:idA = 2212");
  pythia.settings.parm("Beams:eA", eProton);
  // BeamB = electron.
  pythia.readString("Beams:idB = 11");
  pythia.settings.parm("Beams:eB", eElectron);
  // pythia.settings.parm("Beams:eCM", 300);


  // Set up DIS process within some phase space.
  // Neutral current (with gamma/Z interference).
  pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");
  // Uncomment to allow charged current.
  pythia.readString("WeakBosonExchange:ff2ff(t:W) = on");
  // Phase-space cut: minimal Q2 of process.
  pythia.settings.parm("PhaseSpace:Q2Min", Q2min);

  // Set dipole recoil on. Necessary for DIS + shower.
  pythia.readString("SpaceShower:dipoleRecoil = on");

  // Allow emissions up to the kinematical limit,
  // since rate known to match well to matrix elements everywhere.
  pythia.readString("SpaceShower:pTmaxMatch = 2");

  // QED radiation off lepton not handled yet by the new procedure.
  pythia.readString("PDF:lepton = off");
  pythia.readString("TimeShower:QEDshowerByL = off");

  // Lower the effect of transverse momentum  and resulting particle mass cut-offs 
  //(can't be turned off completely).
  pythia.readString("PhaseSpace:mHatMin = force 0.1");
  pythia.readString("PhaseSpace:pTHatMinDiverge = force 0.1");

  // Turn parton or hadron levels off
  // pythia.readString("PartonLevel:all = off"); // Don't turn this and others off
  pythia.readString("PartonLevel:FSR = off");
  pythia.readString("PartonLevel:ISR = off");
  pythia.readString("HadronLevel:all = off");
  pythia.readString("PartonLevel:Remnants = off");
  pythia.readString("Check:event = off");

  // Try different kinematics
  pythia.readString("PhaseSpace:DISkinmode = 1");

  // Initialize.
  pythia.init();

  // Histograms.
  double Wmax = sqrt(4.* eProton * eElectron);
  Hist Qhist("Q [GeV]", 100, 0., 25.);
  Hist Whist("W [GeV]", 100, 0., Wmax);
  Hist xhist("x", 100, 0.00001, 1., true);
  // Hist xhist("x", 100, 0., 1.);
  Hist yhist("y", 100, 0., 1.);
  Hist pTehist("pT of scattered electron [GeV]", 100, 0., 25.);
  Hist pTrhist("pT of radiated parton [GeV]", 100, 0., 25.);
  Hist pTdhist("ratio pT_parton/pT_electron", 100, 0., 5.);
  Hist xdiffhist("x - x1", 100, 0., 0.01);

  ofstream myfile;
  myfile.open("myout36");
  myfile << "# Contents: Bjorken x, energy scale Q^2, y, W, pHatT, Q22, x1" << endl;

  // Begin event loop.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;

    // Four-momenta of proton, electron, virtual photon/Z^0/W^+-.
    Vec4 pProton = event[1].p(); // 1 For swapped beams, indices 2
    Vec4 peIn    = event[2].p(); // 2                            1
    Vec4 peOut   = event[6].p(); // 6                            5
    Vec4 pPhoton = peIn - peOut;

    // Q2, W2, Bjorken x, y.
    double Q2    = - pPhoton.m2Calc();
    double W2    = (pProton + pPhoton).m2Calc();
    double x     = Q2 / (2. * pProton * pPhoton);
    double y     = (pProton * pPhoton) / (pProton * peIn);
    double x1    = pythia.info.x1();
    double xdiff = x - x1;
    double Q22   = -pythia.info.tHat();

    Qhist.fill( sqrt(Q2) );
    Whist.fill( sqrt(W2) );
    xhist.fill( x );
    yhist.fill( y );
    pTehist.fill( event[6].pT() );
    pTrhist.fill( event[5].pT() );
    xdiffhist.fill( xdiff );                        

    // Print accepted try index, x and Q2 to file.
    myfile << x << " " << Q2 << " " << y << " " << W2 << " " << event[5].pT() << " " << Q22 << " " << x1 << endl;

    cout << "proton:  " << pProton << "eIn:     " << peIn         << "eOut:    " << peOut
         << "q:       " << pPhoton << "quarkIn: " << event[3].p() << "quarkOut:" << event[5].p() << endl; 
         // Proton beam A: quarkIn 3, quarkOut 5. For swapped beams quarkIn 4, quarkOut 6. Check event list.

    event.list();
    pythia.process.list();

  // End of event loop. Statistics and histograms.
  }
  myfile.close();

  pythia.stat();

  // xhist.normalizeSpectrum(nEvent);

  // Qhist.normalize();
  // Whist.normalize();
  // xhist.normalize();
  // yhist.normalize();
  // pTehist.normalize();
  // pTrhist.normalize();
  // xdiffhist.normalize();                      
  


  cout << Qhist << Whist << xhist << yhist << pTehist << pTrhist << xdiffhist << endl << endl;
  

  // Done.
  return 0;
}
